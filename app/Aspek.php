<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aspek extends Model
{
    //
    protected $table = 'aspek';
    protected $fillable = [
        'nama_aspek'
    ];

    public function pertanyaanddst() {
        return $this->hasMany(PertanyaanDdst::class);
    }
    public function jawabanresponden() {
        return $this->hasMany(JawabanResponden::class);
    }
}
