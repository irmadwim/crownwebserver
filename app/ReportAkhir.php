<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportAkhir extends Model
{
    //
    protected $table = 'jawaban_responden';
    protected $fillable = [
        'hasil_akhir'
    ];
    public function anak() {
        return $this->belongsTo(Anak::class);
    }
    public function jawabanresponden() {
        return $this->belongsTo(JawabanResponden::class);
    }
}
