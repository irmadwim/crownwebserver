<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PertanyaanDdst extends Model
{
    //
    protected $table = 'pertanyaan_ddst';
    protected $fillable = [
        'usia', 'pertanyaan'
    ];
    public function aspek() {
        return $this->belongsTo(Aspek::class);
    }
}
