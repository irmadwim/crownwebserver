<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataGizi extends Model
{
    //
    protected $table = 'data_gizi';
    protected $fillable = [
        'tb_anak', 'bb_anak', 'lk_anak'
    ];
    public function anak() {
        return $this->belongsTo(Anak::class);
    }
    public function hasilgizi(){
        return $this->hasOne(HasilGizi::class);
    }
}
